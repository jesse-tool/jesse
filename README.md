## Jesse
Jesse is a program analysis tool which returns for a given binary (program or
library) all system calls it can invoke.

### Example
In `example/`, Jesse is used to analyze which system calls can be invoked by
the libc in Debian buster:

    $ cd example/
    $ make

See `data/e63efc14f34504f4ac4cf7d63ed229ca/syscalls.jesse.json` for the results
of the analysis.

### Requirements
pyelftools, capstone, angr, claripy, PyGObject, intervaltree

## Paper for reference:
Please consider citing our paper if you found Jesse useful.
```
@inproceedings{jesse_groh:2023,
    title        = {{Free Willy: Prune System Calls to Enhance Software Security}},
    author       = {Groh, Charlie and Proskurin, Sergej and Zarras, Apostolis},
    booktitle    = {ACM Symposium on Applied Computing (SAC)},
    year         = {2023}
}
```
