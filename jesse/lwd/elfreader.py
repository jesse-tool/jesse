# Copyright (C) 2022 Charlie Groh
# This file is part of Jesse.
#
# Jesse is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Jesse is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Jesse. If not, see <https://www.gnu.org/licenses/>.

# Copyright (c) 2017, Alexis Engelke
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

from capstone.x86 import *
from intervaltree import IntervalTree, Interval
import io
import os

from capstone import *
from elftools.elf.elffile import ELFFile
from elftools.elf.relocation import RelocationSection
from elftools.elf.sections import SymbolTableSection
from elftools.elf.constants import SH_FLAGS
from elftools.common.utils import parse_cstring_from_stream

class ELFReader(object):
    def __init__(self, binary, flavor):
        self.flavor = flavor
        self.fd = io.BytesIO(binary)
        self.elf = ELFFile(self.fd)
        self.arch = self.elf.get_machine_arch()
        if self.arch == "x64":
            self.cs = Cs(CS_ARCH_X86, CS_MODE_64)
        elif self.arch == "x86":
            self.cs = Cs(CS_ARCH_X86, CS_MODE_32)
        else:
            raise Exception("unknown arch")
        self.cs.detail = True
        self.vaddr = 0
    def vseek(self, addr):
        if len(list(self.elf.address_offsets(addr))) == 0:
            print("Address outside of binary: " + hex(addr))
        fileoff = list(self.elf.address_offsets(addr))[0]
        self.fd.seek(fileoff, os.SEEK_SET)
        self.vaddr = addr
    def vread(self, length):
        data = self.fd.read(length)
        self.vaddr += len(data)
        return data
    def vreadInstr(self):
        stream = self.fd.read(32)
        try:
            disas = list(self.cs.disasm(stream, self.vaddr, 1))[0]
        except Exception:
            print("Cannot decode:", stream, hex(self.vaddr))
        self.vseek(self.vaddr + disas.size)
        return Instruction(disas)
    def vreadCSInstr(self):
        stream = self.fd.read(32)
        try:
            disas = list(self.cs.disasm(stream, self.vaddr, 1))[0]
        except Exception:
            print("Cannot decode:", stream, hex(self.vaddr))
            exit(1)
#        self.vseek(self.vaddr + disas.size)
        return disas
    def get_flavor(self):
        return self.flavor
    def get_symbol(self, symbolName):
        symtab = self.elf.get_section_by_name(".symtab" if self.flavor == "strong" else ".dynsym")
        if not symtab or not isinstance(symtab, SymbolTableSection):
            return
        for i in range(symtab.num_symbols()):
            symbol = symtab.get_symbol(i)
            if symbol.name != symbolName: continue
            return symbol["st_value"]
    def get_section(self, name):
        # for i in range(self.elf.num_sections()): print(self.elf.get_section(i).name)
        return self.elf.get_section_by_name(name)
    def is_executable_address(self, address):
        if type(address) is not int:
            return False
        for i in range(self.elf.num_sections()):
            section = self.elf.get_section(i)
            if section['sh_flags'] & SH_FLAGS.SHF_EXECINSTR:
                start = section['sh_addr']
                end = section['sh_addr'] + section['sh_size']
                if start <= address and end > address:
                    return True
        return False
    def executable_addresses(self):
        it = IntervalTree()
        for i in range(self.elf.num_sections()):
            section = self.elf.get_section(i)
            if section['sh_flags'] & SH_FLAGS.SHF_EXECINSTR:
                it.add(Interval(section['sh_addr'], section['sh_addr'] + section['sh_size']))
        it.merge_overlaps()
        return it

    def iter_symbols(self):
        for tableName in (".symtab", ".dynsym"):
            symtab = self.elf.get_section_by_name(tableName)
            if symtab and isinstance(symtab, SymbolTableSection):
                for symbol in symtab.iter_symbols():
                    # print(symbol.name, symbol["st_info"], symbol["st_value"])
                    if symbol["st_value"] == 0:
                        continue
                    if symbol["st_info"]["type"] == "STT_FUNC" or symbol["st_info"]["type"] == "STT_LOOS":
                        yield symbol["st_value"], symbol.name, "function"
                    if symbol["st_info"]["type"] == "STT_OBJECT":
                        yield symbol["st_value"], symbol.name, "object"

    def get_string(self, address):
        return parse_cstring_from_stream(self.elf.stream, address)
