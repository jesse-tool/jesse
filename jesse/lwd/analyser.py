# Copyright (C) 2022 Charlie Groh
# This file is part of Jesse.
#
# Jesse is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Jesse is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Jesse. If not, see <https://www.gnu.org/licenses/>.

from lwd.model import Model, OperandType, OperandKind, CF_INSTR_COND, CF_INSTR_UNCOND
from intervaltree import IntervalTree, Interval
import angr, claripy
from angr import sim_options
import os, sys
import bisect, binascii, hashlib, logging, json

logging.basicConfig(format='%(message)s')
logger = logging.getLogger("analyser")
logger.setLevel(logging.INFO)

imports = {}
exports = {}
functions = {}
blocks = {}
syscalls = {}
calls = {}
dlsym_calls = set()
it = IntervalTree()
transitions = set()

class DlsymCall(object):
	def __init__(self, offset, block, function):
		self.offset = offset
		self.block = block
		self.function = function

class Call(object):
	def __init__(self, offset, block, size):
		self.offset = offset
		self.block = block
		self.size = size

class Syscall(object):
	def __init__(self, offset, block, size, calls_libc):
		self.offset = offset
		self.block = block
		self.size = size
		self.calls_libc = calls_libc
		self.number = None

class Block(object):
	def __init__(self, offset, end, successors):
		self.offset = offset
		self.end = end
		self.predecessors = set()
		self.successors = successors
		self.postcondition = None
		self.syscalls = []
		self.calls = []
		self.live = True

	def __getattr__(self, k):
		if k == 'syscall_numbers':
			return set([syscall.number for syscall in self.syscalls])

class Function(object):
	def __init__(self, offset):
		self.offset = offset
		self.blocks = set()
		self.external = set()
		self.internal = set()

	def __getattr__(self, k):
		if k == 'syscall_numbers':
			if not self.blocks:
				return set()
			return set.union(*[block.syscall_numbers for block in self.blocks])

def evaluate_register_values(begin, end, registers, values):
	simstate = angr_project.factory.blank_state(addr=(begin + (0x400000 if angr_project.loader.main_object.pic else 0)), add_options={sim_options.SYMBOL_FILL_UNCONSTRAINED_MEMORY, sim_options.SYMBOL_FILL_UNCONSTRAINED_REGISTERS})
	for reg in values:
		simstate.regs.__setattr__(reg, values[reg])
	sm = angr_project.factory.simulation_manager(simstate)
	sm.explore(find=(end + (0x400000 if angr_project.loader.main_object.pic else 0)))
	if not sm.found:
		return None
	values = {}
	for reg in registers:
		values[reg] = sm.found[0].solver.eval(sm.found[0].regs.__getattr__(reg))
	ret = {}
	for reg in registers:
		simstate = sm.found[0].copy()
		simstate.solver.add(simstate.regs.__getattr__(reg) != values[reg])
		if not simstate.solver.satisfiable():
			ret[reg] = values[reg]
	return ret

def analyse_execution(block, end, end_is_inside_block, registers, values):
	global calls
	global syscalls

	types = {addr.offset: "call" for addr in block.calls}
	types.update({addr.offset: "syscall" for addr in block.syscalls})
	if end_is_inside_block == True:
		types[end] = "end"
	keys = list(types.keys())
	keys.sort(reverse=True)
	if end_is_inside_block == False:
		keys.insert(0, end)
		types[end] = "end"
	begin = block.offset
	register_values = values
	while keys:
		addr = keys.pop()
		if types[addr] == "call":
			# based on x86-64 calling convention
			regs = set(['rbx', 'rbp', 'r12', 'r13', 'r14', 'r15'])
		elif types[addr] == "syscall":
			# based on REGISTERS_CLOBBERED_BY_SYSCALL in sysdeps/unix/sysv/linux/x86_64/sysdep.h in glibc
			regs = set(['rbx', 'rbp', 'rdx', 'rsi', 'rdi', 'r8', 'r9', 'r10', 'r12', 'r13', 'r14', 'r15'])
		elif types[addr] == "end":
			regs = registers
		# mitigate angr "bug"
		register_values = dict(evaluate_register_values(begin, addr, regs, register_values).items() & evaluate_register_values(begin, addr, regs, register_values).items())
		if types[addr] == "call":
			begin = addr + calls[addr].size
		elif types[addr] == "syscall":
			begin = addr + syscalls[addr].size
		elif types[addr] == "end":
			return register_values

def compute_precondition(block):
	global blocks

	if not block.predecessors:
		if block.live:
			return {}
		else:
			return None
	postconditions = [set(blocks[pre].postcondition.items()) for pre in block.predecessors if blocks[pre].postcondition != None]
	if not postconditions:
		return None
	return dict(set.intersection(*postconditions))

def simple_register_analysis(block, end, end_is_inside_block, register):
	register_values = analyse_execution(block, end, end_is_inside_block, set([register]), {})
	if register in register_values:
		return register_values[register]
	else:
		return None

def analyse_syscall(syscall):
	global blocks

	logger.info("Analyzing syscall at address " + hex(syscall.offset))
	if syscall.calls_libc:
		syscall_number_register = 'rdi'
	else:
		syscall_number_register = 'rax'
	try:
		# try simplest case first
		syscall_number = simple_register_analysis(syscall.block, syscall.offset, True, syscall_number_register)
		if not syscall_number == None:
			return syscall_number

		# try harder
		visited = set([syscall.block])
		terminators = set([syscall.block])
		while True:
			update = set()
			for block in terminators:
				update |= set(blocks[pre] for pre in block.predecessors if blocks[pre] not in visited)
			terminators = update
			for block in terminators:
				block.postcondition = {}
			for block in visited:
				block.postcondition = None
			stable = False
			while not stable:
				stable = True
				for block in visited:
					# update block postcondition
					precondition = compute_precondition(block)
					if precondition == None:
						continue
					register_values = analyse_execution(block, block.end, False, set(['rax', 'rbx', 'rcx', 'rdx', 'rbp', 'rsi', 'rdi', 'r8', 'r9', 'r10', 'r11', 'r12', 'r13', 'r14', 'r15']), precondition)
					stable = stable and block.postcondition == register_values
					block.postcondition = register_values
			for block in visited:
				print(hex(block.offset))
				print(block.postcondition)
			precondition = compute_precondition(syscall.block)
			if precondition != None:
				register_values = analyse_execution(syscall.block, syscall.offset, True, set([syscall_number_register]), precondition)
				if syscall_number_register in register_values:
					return register_values[syscall_number_register]
			if not terminators:
				return None
			visited |= terminators
	except Exception as e:
		print(e)
		logger.error("Error in syscall analysis - probably a bug in angr")
		return None

def mk(value, dictionary):
	if value.offset not in dictionary:
		dictionary[value.offset] = value
		ret = value
	else:
		ret = dictionary[value.offset]
	return ret

def should_ignore(address, ignore):
	for interval in ignore:
		if interval[0] <= address and interval[1] > address:
			logger.info("Ignored address " + hex(address))
			return True
	return False

def analyse_function(address, noreturn, ignore):
	global imports
	global functions
	global blocks
	global syscalls
	global calls
	global dlsym_calls
	global it
	global transitions

	if address in functions:
		return
	function = Function(address)
	functions[address] = function
	logger.info("Analyzing function at address " + hex(address))
	f = lwd_project.get_function(address)
	# create CFG
	for b in f.basicBlocks:
		s = set()
		if 'fallthrough' in b.successors:
			s.add(b.successors['fallthrough'])
		if 'jump' in b.successors:
			s.add(b.successors['jump'])
		it.chop(b.address, b.instructions[-1].address + b.instructions[-1].size)
		if b.instructions[-1].id in CF_INSTR_COND or b.instructions[-1].id in CF_INSTR_UNCOND:
			end = b.instructions[-1].address
		else:
			end = b.instructions[-1].address + b.instructions[-1].size
		block = mk(Block(b.address, end, s), blocks)
		if b.instructions[0].mnemonic == 'nop':
			block.live = False
		function.blocks.add(block)
		lastInstructionIsNop = False
		for o in b.instructions:
			if o.mnemonic == 'nop':
				lastInstructionIsNop = True
			else:
				if lastInstructionIsNop:
					transitions.add(o.address)
				lastInstructionIsNop = False

			if o.mnemonic == 'call':
				block.calls.append(mk(Call(o.address, block, o.size), calls))
				if o.operands[0].type == OperandType.IMM and o.operands[0].imm in noreturn:
					block.successors = set()
			elif o.mnemonic == 'syscall':
				block.syscalls.append(mk(Syscall(o.address, block, 2, False), syscalls))

			if o.mnemonic == 'call' or o.mnemonic == 'jmp':
				operand = o.operands[0]
			elif lwd_project.get_flavor() == "strong" and (o.mnemonic == 'mov' or o.mnemonic == 'lea'):
				operand = o.operands[1]
			else:
				continue
			if operand.type == OperandType.IMM and (o.mnemonic == 'call' or o.mnemonic == 'jmp'):
				dest = operand.imm
			elif lwd_project.get_flavor() == "strong" and operand.type == OperandType.MEM and operand.kind == OperandKind.ADDR:
				dest = operand.mem.disp
				if not o.mnemonic == 'lea':
					dest = lwd_project.get_relocation(dest)
			else:
				continue
			if lwd_project.get_flavor() == "strong" and type(dest) is int and not lwd_project.is_executable_address(dest):
				dests = lwd_project.get_relocations(dest)
			else:
				dests = set([dest])
			for dest in dests:
				if type(dest) is int and  dest in imports:
					dest = imports[dest]
				if type(dest) is int:
					if not should_ignore(dest, ignore):
						function.internal.add(dest)
						analyse_function(dest, noreturn, ignore)
				elif type(dest) is str or type(dest) is bytes:
					function.external.add(dest)
					if dest == "syscall":
						block.syscalls.append(mk(Syscall(o.address, block, o.size, True), syscalls))
					elif dest == "dlsym":
						dlsym_calls.add(DlsymCall(o.address, block, function))

def md5sum(file):
	md5 = hashlib.md5()
	with open(file, 'rb') as f:
		while True:
			data = f.read(65536)
			if not data:
				break
			md5.update(data)
	return md5.hexdigest()

def gather_results(function, syscall_numbers, external_functions, visited):
	if function in visited:
		return
	visited.add(function)
	syscall_numbers |= functions[function].syscall_numbers
	external_functions |= functions[function].external
	for f in functions[function].internal:
		gather_results(f, syscall_numbers, external_functions, visited)

def analyse_binary(binary):
	global angr_project
	global lwd_project
	global imports
	global exports
	global it
	global transitions

	data_path = os.path.join(os.getcwd(), "data")
	if not os.path.isdir(data_path):
		logging.error(data_path + " does not exist!")
		exit(1)
	if os.path.exists(os.path.join(data_path, "strong")):
		flavor = "strong"
	elif os.path.exists(os.path.join(data_path, "weak")):
		flavor = "weak"
	else:
		logging.error("Neither " + data_path + "/strong nor " + data_path + "/weak exists!")
		exit(1)
	data_path = os.path.join(data_path, md5sum(binary))
	if not os.path.isdir(data_path):
		logging.error(data_path + " does not exist!")
		exit(1)
	conf_path = os.path.join(data_path, "meta.json")
	if not os.path.exists(conf_path):
		logging.error(data_path + "/meta.json does not exist!")
		exit(1)
	with open(conf_path) as f:
		conf = json.load(f)

	with open(os.path.join(data_path, conf['binary']), "br") as f:
		binaryFile = f.read()
	lwd_project = Model(binaryFile, flavor, set(conf['noreturn']))
	it = lwd_project.elf.executable_addresses()
	imports = lwd_project.get_imports()
	exports = lwd_project.get_exports()
	if flavor == "strong":
		init_array = lwd_project.get_init_array()
		fini_array = lwd_project.get_fini_array()

	ignore = []
	if 'ignore' in conf:
		ignore = conf['ignore']

	for symbol in exports:
		if should_ignore(exports[symbol], ignore):
			continue
		analyse_function(exports[symbol], set(conf['noreturn']), ignore)

	if flavor == "strong":
		for symbol in init_array | fini_array:
			if type(symbol) is int:
				if should_ignore(symbol, ignore):
					continue
				analyse_function(symbol, set(conf['noreturn']), ignore)

	for i in ignore:
		it.chop(i[0], i[1])
	while not it.is_empty():
		for i in it.copy():
			if i.begin % 16 == 0:
				analyse_function(i.begin, set(conf['noreturn']), ignore)
				continue
			if i.begin + 16 - i.begin % 16 < i.end:
				if lwd_project.only_zeros(i.begin, 16 - i.begin % 16):
					it.chop(i.begin, i.begin + 16 - i.begin % 16)
					analyse_function(i.begin + 16 - i.begin % 16, set(conf['noreturn']), ignore)
				else:
					analyse_function(i.begin, set(conf['noreturn']), ignore)
				continue
			if lwd_project.only_zeros(i.begin, i.end - i.begin):
				it.chop(i.begin, i.end)
			else:
				analyse_function(i.begin, set(conf['noreturn']), ignore)

	for i in transitions.copy():
		if i not in blocks:
			analyse_function(i, set(conf['noreturn']), ignore)

	for b in blocks:
		for suc in blocks[b].successors:
			if suc != 0:
				blocks[suc].predecessors.add(b)

	logger.info("Found " + str(len(syscalls)) + " syscalls")

	if os.path.exists(os.path.join(data_path, "syscalls.json")):
		with open(os.path.join(data_path, "syscalls.json"), "r") as f:
			syscall_numbers = json.load(f)
		for syscall in syscall_numbers:
			syscalls[int(syscall)].number = syscall_numbers[syscall]
	angr_project = angr.Project(os.path.join(data_path, conf['binary']))
	if flavor == "strong" and not angr_project.loader.main_object.pic:
		logger.error("Binary must be position-independent for strong analysis!")
		exit(1)
	for syscall in syscalls:
		if syscall not in conf['blacklist'] and syscalls[syscall].number == None:
			syscalls[syscall].number = analyse_syscall(syscalls[syscall])
		logger.info(hex(syscall) + ": " + str(syscalls[syscall].number))

	out = {}
	for syscall in syscalls:
		out[syscall] = syscalls[syscall].number if not syscalls[syscall].number or (syscalls[syscall].number >= 0 and syscalls[syscall].number <= 334) else None
	with open(os.path.join(data_path, "syscalls.jesse.json"), "w") as f:
		json.dump(out, f)

	if flavor == "weak":
		exit(0)

	for function in functions:
		external = set()
		for e in functions[function].external:
			if type(e) is bytes:
				external.add(e.decode("ascii"))
			else:
				external.add(e)
		functions[function].external = external

	out = {}
	for function in functions:
		out[function] = {'syscalls': list(functions[function].syscall_numbers), 'internal': list(functions[function].internal), 'external': list(functions[function].external)}
	with open(os.path.join(data_path, "callgraph.json"), "w") as f:
		json.dump(out, f)

	out = {}
	for function in exports:
		if should_ignore(exports[function], ignore):
			continue
		syscall_numbers = set()
		external_functions = set()
		gather_results(exports[function], syscall_numbers, external_functions, set())
		out[function] = {'syscalls': list(syscall_numbers), 'imports': list(external_functions)}
	if "init_array" in out or "fini_array" in out:
		logging.error("init_array or fini_array already defined!")
	else:
		for array in [[init_array, "init_array"], [fini_array, "fini_array"]]:
			syscall_numbers = set()
			external_functions = set()
			for function in array[0]:
				if type(function) is str:
					external_functions.add(function)
				elif type(function) is int:
					if should_ignore(function, ignore):
						continue
					gather_results(function, syscall_numbers, external_functions, set())
			out[array[1]] = {'syscalls': list(syscall_numbers), 'imports': list(external_functions)}
	with open(os.path.join(data_path, "result.json"), "w") as f:
		json.dump(out, f)
